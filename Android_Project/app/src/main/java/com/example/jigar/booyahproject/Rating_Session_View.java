package com.example.jigar.booyahproject;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.example.jigar.booyahproject.rest.model.request.RatingsRequest;
import com.example.jigar.booyahproject.rest.model.response.AuthTokenResponse;
import com.example.jigar.booyahproject.rest.model.response.RatingsResponse;
import com.example.jigar.booyahproject.rest.model.response.MediaResponse;

/**
 * When switching to this view, assume we have all the data needed to star the rating session right away.
 *
 */
public class Rating_Session_View extends AppCompatActivity {

    private static Rating_Session_Controller session;
    private TextView txtTimeElapsed, txtMediaName;
    private int startTimeOffset;
    private Intent intent;
    private RatingsResponse goodSecsPassed, badSecsPassed;
    private RatingsRequest goodSecs, badSecs;
    private Button btnDone;
    private ImageButton btnBoo, btnYah, btnPause, btnFF, btnRR;
    private Bundle bundle;
    private AuthTokenResponse authToken;
    private MediaResponse media;
    private static final String boo = "Boo!";
    private static final String yah = "Yah!";

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rating_screen);

        intent = getIntent();
        bundle = intent.getExtras();

        Toolbar navToolbar = findViewById(R.id.headerNav);
        setSupportActionBar(navToolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.show();

        // see: https://stackoverflow.com/a/47975023
        // fixes bug where navToolbar creates new parent activity
        navToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        //Get data from the intent's bundle.
        authToken = (AuthTokenResponse) bundle.getSerializable(Constants.AUTH_TOKEN);
        media = (MediaResponse) bundle.getSerializable(Constants.MEDIA_ITEM);
        startTimeOffset = bundle.getInt(Constants.TIME_OFFSET, 0); //Will be 0 if none was passed in.
        goodSecsPassed = (RatingsResponse) bundle.getSerializable(Constants.LIKES);
        badSecsPassed = (RatingsResponse) bundle.getSerializable(Constants.DISLIKES);

        if (media == null || media.getId() == 0 || media.getDuration() == 0) {
            //TODO: Just disable functionality
            Toast.makeText(getApplicationContext(), Constants.TOAST_ERROR, Toast.LENGTH_SHORT).show();
            Log.e("Starting page", "A piece of the media object was not set.");
            finish();
        }

        //Create the ratingRequests that will have seconds added to them. Different constructor based on whether or not there's existing data.
        if (badSecsPassed == null || badSecsPassed.seconds == null){
            badSecs = new RatingsRequest();
        } else {
            badSecs = new RatingsRequest(badSecsPassed.seconds);
        }

        if (goodSecsPassed == null || badSecsPassed.seconds == null) {
            goodSecs = new RatingsRequest();
        } else {
            goodSecs = new RatingsRequest(goodSecsPassed.seconds);
        }

        //Find all page elements
        txtMediaName = findViewById(R.id.mediaTitle);
        txtTimeElapsed = findViewById(R.id.timerDisplay);
        btnPause = findViewById(R.id.btnPlay);
        btnFF = findViewById(R.id.btnForward);
        btnRR = findViewById(R.id.btnRewind);

        btnBoo = findViewById(R.id.btnBad);
        btnYah = findViewById(R.id.btnGood);
        btnDone = findViewById(R.id.btnFinished);


        //Set labels
        txtMediaName.setText(media.getName());

        //START THE SESSION
        session = new Rating_Session_Controller(authToken, startTimeOffset, goodSecs, badSecs, media.getDuration(), media.getId());

        //Set labels
        txtMediaName.setText(media.getName() + " - " + media.getAuthor());
        //Ensure pause button has correct icon.
        btnPause.setImageResource(R.drawable.ic_rating_pause);

        //Allow displayed timer to update.
        Rating_Session_View_Updater uiUpdater = new Rating_Session_View_Updater(session, this);
        Timer timer = new Timer();

        timer.schedule(uiUpdater, 100, 100);    //UI refreshes every 0.1 seconds.

        //--    Set up button actions.  --//
        btnFF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.fastForward(10);
                if (session.atMaxTime()) {
                    btnPause.setEnabled(false);
                }
            }
        });

        /**
         * Move timer 10 seconds forward, and ensure buttons are in correct state.
         */
        btnRR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (session.isPaused()) {  //We know we are paused and will want to continue manually.
                    btnPause.setImageResource(R.drawable.ic_rating_play);
                }

                btnFF.setEnabled(true);
                btnPause.setEnabled(true);
                session.reWind(10);
            }
        });

        btnPause.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (session.isPaused()) {       //If currently paused, play
                    session.resumeTimer();
                    btnPause.setImageResource(R.drawable.ic_rating_pause);
                    btnDone.setEnabled(false);
                    btnDone.setBackgroundColor(ContextCompat.getColor(Rating_Session_View.this, R.color.colorAshGrey));
                } else {                        //If currently playing, pause
                    session.pauseTimer();
                    btnPause.setImageResource(R.drawable.ic_rating_play);
                    btnDone.setEnabled(true);       //User can now finish the rating session.
                    btnDone.setBackgroundColor(ContextCompat.getColor(Rating_Session_View.this, R.color.colorYellow));
                }
            }
        });

        btnBoo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnBoo.setImageResource(R.drawable.ic_rating_down_solid);
                session.addBadRating();
                Toast.makeText(Rating_Session_View.this, boo, Toast.LENGTH_SHORT).show();
                //btnBoo.setImageResource(R.drawable.ic_rating_down);
            }
        });

        btnYah.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                btnYah.setImageResource(R.drawable.ic_rating_up_solid);
                session.addGoodRating();
                Toast.makeText(Rating_Session_View.this, yah , Toast.LENGTH_SHORT).show();
                //btnYah.setImageResource(R.drawable.ic_rating_up);
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDone.setEnabled(false);      //Prevent double-clicking
                btnDone.setBackgroundColor(ContextCompat.getColor(Rating_Session_View.this, R.color.colorAshGrey));
                session.finishRating();
                finish();
            }
        });
        btnDone.setEnabled(false);      //User can not end the session until they pause, or timer is finished.
        btnDone.setBackgroundColor(ContextCompat.getColor(Rating_Session_View.this, R.color.colorAshGrey));
    }

    //--    Wrapper methods for the View updater to call on the UI thread.  --//
    /**
     * @param updatedTime Formatted time that is to the displayed.
     */
    protected void updateTimerLabel(String updatedTime) {
        txtTimeElapsed.setText(updatedTime);
        btnYah.setImageResource(R.drawable.ic_rating_up);
        btnBoo.setImageResource(R.drawable.ic_rating_down);
    }

    protected void setMaxTimeState() {
        btnDone.setEnabled(true);
        btnDone.setBackgroundColor(ContextCompat.getColor(Rating_Session_View.this, R.color.colorYellow));
        btnPause.setEnabled(false);
        btnPause.setImageResource(R.drawable.ic_rating_play);
        btnFF.setEnabled(false);
    }
}

/**
 *  Update the UI every 0.1 seconds.
 *  Not using android.os.countDownTimer because we can't know how long the session will be (i.e. if they pause, RR, or FF).
 *  Calls wrapper methods from the view, using the uiThread, Since other threads can't access the view by themselves
 */
class Rating_Session_View_Updater extends TimerTask{
    private Rating_Session_Controller session;
    private String formattedTime;
    private Rating_Session_View view;
    private boolean likeOrDislikeState;   //Whether or not we are showing a like or dislike state.

    /**
     * @param session   The rating session's controller
     * @param v         View that will be updated.
     */
    public Rating_Session_View_Updater(Rating_Session_Controller session, Rating_Session_View v) {
        this.session = session;
        this.view = v;
        likeOrDislikeState = false;
        Log.d("Time Update process", "Created updater");
    }

    @Override
    public void run() {
        //Step 1: Update timer.
        formattedTime = DateUtils.formatElapsedTime(session.getSecondsElapsed());
        view.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                view.updateTimerLabel(formattedTime);
            }
        });

        //Step 2: If at max time state and not yet paused.
        if (!session.isPaused() && session.atMaxTime()) {
            session.pauseTimer();
            view.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    view.setMaxTimeState();
                }
            });
        }


        //Step 3: Update UI to show that this moment is "liked"
        //TODO: Determine how to show it's liked or disliked.
        if (session.currSecondGood()) {
            view.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //TODO: Show it's good.
                }
            });
            likeOrDislikeState = true;
        } else if (session.currSecondBad()) {
            view.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //TODO: Show it's bad.
                }
            });
            likeOrDislikeState = true;
        } else if (likeOrDislikeState){       //Only do this if we need to return to normal state.
            view.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //TODO: Return UI to normal.
                }
            });
            likeOrDislikeState = false;
        }
    }
}