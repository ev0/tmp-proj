# **Booyah! - The moment-based media rating application**
Group 5 final Project for Dalhousie University's Fall 2018 edition of CSCI 4176/5708: Mobile Computing.

**Instructor**: Dr. Tami Meredith

## **Members**
- Jigar Joshi (B00812722)
- Shivani Desai (B00799169)
- Bola Okesanjo (B00876268)
- Vivek Shah (B00799155)
- Ryan Stevens (B00695460)

## **GitLab Repository**
GitLab repository at https://git.cs.dal.ca/rstevens/booyah

**Important:** In order to use *Booyah!*, the server must be running. Please contact Bola Okesanjo at o.okesanjo@dal.ca or (902-448-0790) to arrange a time for it to be running.

## **Project Summary**

*BooYah!* is a time-based media rating application for Android, that allows users to create quantitative feedback on particular moments of movies, tv shows, music, similar time-based media. Users rate particularly impactful moments bad ("Boo!") or good ("Yah!"). Ratings for a BooYah! session are assembled into graphs that show the parts that the user liked and disliked. When multiple people rate a piece of media, those ratings are aggregated to show the total number of likes and dislikes for moments of the media.

## Audience, Purpose, and Benefits

Popular media rating platforms like https://www.rottentomatoes.com/ provide quantitative (numeric) data audience's overall impressions about media, but provide only qualitative (written) feedback about **what** makes the media so great or dismal. Content viewers must then manually parse the feedback from other users to determine if it has elements they will like a dislike. Similarly, content creators will need to carefully read that information to learn how to create better content in the future. *BooYah!* solves this problem by allowing viewers to quickly see the moments that were liked and disliked by the audience.

Users of *Booyah!* will do so with any combination of these three goals in mind:
- Rate Media

	Typical users, mainly teens and young adults, will use BooYah! as a way to express their opinions about media they consume. They will sit down in their homes with the media, and the application open beside them. When they find themselves liking or disliking a particular moment, they will express that emotion by pressing "Boo" or "Yah". After the session, they will likely view the graph they have generated and reflect on how good (or bad) the media they just consumed was. If the user wants to share their rating graph on social media, they will take a screen-shot of their device and share that photo.
	
- See opinions to determine if they want to consume the media

	Other primary users, mainly teens, young adults, and tech-savy adults, will use BooYah! to decide whether or not they wish to consume a particular piece of media. They will search for the media and view the aggregate graph of ratings about that media. Based on information they gleam from the graph, they will be able to make an informed decision about whether or not to proceed with consuming the media.

- See opinions so they can produce better media

	Content creators who seek to improve their quality of content will use *BooYah!* to learn what their audience liked and disliked about existing media, and tailor their new content to include more moments like the ones rated good, and fewer moments like the ones rated bad.

## Libraries
The following are the libraries used for the project.

### Android libraries
The following are the Android libraries used to create the mobile application:

- **square-retrofit:** Retrofit is an HTTP client for Android and Java. We chose it over _volley_ because, unlike _volley_, it can send POST multi-part form requests which we needed for automatic media-time recognition. Source [here](https://square.github.io/retrofit/)
  
- **google-gson:** Gson is a Java library that can be used to convert Java Objects into their JSON representation. It can also be used to convert a JSON string to an equivalent Java object. Source [here](https://github.com/google/gson)
  
- **retrofit-gsonconverter:** Gson converter is an Android library used by Retrofit to serialized Java objects to JSON format by using Gson as the serialization layer. Source [here](https://github.com/square/retrofit/tree/master/retrofit-converters/gson)
  
- **material-edittext:** [_shivani put a 2 sentence description here_]. Source [here](https://github.com/rengwuxian/MaterialEditText)

- **android-graphview:** [_jigar put a 2 sentence description here_]. Source [here](http://www.android-graphview.org/)

### Python API libraries
The following are the Python packages used to create the REST API for the project:

- **Flask:** Flask is a web development micro-framework for Python. It is highly extensible using individual libraries or other 3rd-party libraries to perform tasks like database ORM, serialization etc. Source [here](http://flask.pocoo.org/)

- **Flask-sqlalchemy:** Flask-SQLAlchemy is a Python library that integrates Flask with SQLAlchemy. SQLAlchemy is a popular Python ORM for SQL. Source [here](http://flask-sqlalchemy.pocoo.org/2.3/)

- **Flask-marshmallow:** Flask-Marshmallow is a Python library that integrates Flask with Marshmallow. Marshmallow is an ORM-agnostic serialization library for Python objects. Source [here](https://marshmallow.readthedocs.io/en/3.0/#)

- **Flask-jwtextended:** Flask-JWTExtented is a Python library that enables a Flask application to use JSON Web Tokens (JWT tokens). JWT is a JSON based standard for creating HTTP access tokens. Source [here](https://flask-jwt-extended.readthedocs.io/en/latest/)

- **DejaVu:** DejaVu is a Python library that performs audio fingerprinting and automatic audio recognition using a database of cryptographic fingerprints. Source [here](https://github.com/worldveil/dejavu)

- **PyDub:** PyDub is a simple Python library that is used to manipulate audio files. It does this using ffmpeg. Source [here](https://github.com/jiaaro/pydub)

- **PyTube:** PyTube is a lightweight Python library for downloading YouTube videos. Source [here](https://python-pytube.readthedocs.io/en/latest/)

- **Celery:** Celery is a roboust asynchronous task queue for Python. It uses a distributed messaging queue such as Redis or RabbitMQ to pass messages between a set of tasks and an application. We use it to run resource-intensive API requests asynchronously. Source [here](http://www.celeryproject.org/)

- **RabbitMQ:** RabbitMQ is a popular distributed message broker that we use to pass messages between the API and long running tasks. It does this using the Advanced Message Queuing Protocol (AMQP). Source [here](https://www.rabbitmq.com/)

## Installation Notes
None. However please contact Bola Okesanjo at o.okesanjo@dal.ca or (902-448-0790) in order to set up the API for the app.

## Code Examples
_You will encounter roadblocks and problems while developing your project. Share 2-3 'problems' that your team solved while developing your project. Write a few sentences that describe your solution and provide a code snippet/block that shows your solution. Example:_

_**Problem 1: We needed a method to calculate a Fibonacci sequence**_

_A short description._
```
// The method we implemented that solved our problem
public static int fibonacci(int fibIndex) {
    if (memoized.containsKey(fibIndex)) {
        return memoized.get(fibIndex);
    } else {
        int answer = fibonacci(fibIndex - 1) + fibonacci(fibIndex - 2);
        memoized.put(fibIndex, answer);
        return answer;
    }
}

// Source: Wikipedia Java [1]
```

## Feature Section

1. Sound recognition to identify time indexes in select media
This feature recognizes the media using voice recorder. It generates the hash table and tries to match with existing media available in the media database. On recognizing the media, the user is redirected to rate and review ratings for that media content.

2. Start, pause, and adjust time on a timer
The user will be able to control the timer while rating any media at any specific moment of time. The time when the timer is adjusted will be tracked and synced with the video.

3. Create new media for rating
The application allows the user to add a media entry which is not already in the database and rate it. The user can enter a YouTube URL, and the APi will fetch the video and the duration of that media.
	
4. Search for existing media entries
The media information is stored in the database in JSON format. The user will be able to search the existing media entries to rate them. The search suggestions will be given to the user for the existing media entries. If the media is not detected through sound recognition, the user can manually search for the media

5. View the profile of individual media content
The application shows information for any media content stored in the database. The media name, type, duration, and other information is displayed. The ratings given by all users for the specific media is displayed as a graph.

6. Graph of user's rating in media profile
The ratings given by individual users is stored in the database and represented in the form of a graph. The rating given on each second is reflected on the graph. The users can view this graph for each media content.

7. View graph of aggregate ratings in media profile
The ratings given for a particular media content is aggregated and displayed in the form of a graph. This will give an insight of the ratings given at different time periods by a number of users. The media creaters can use this to find out the ratings of different users for their media content.

8.

## Final Project Status
_Write a description of the final status of the project. Did you achieve all your goals? What would be the next step for this project (if it were to continue)?_

#### Submitted Functionalities
- [ ] _Feature 1 name (Completed)_
- [ ] _Feature 2 name (Partially Completed)_
- [ ] _Feature 3 (Not Implemented)_

## Sources
_What to include in your project sources:_
- Stock images
- Design guides
- Programming tutorials
- Research material
- Android libraries
- Everything listed on the Dalhousie Plagiarism and Cheating pages(https://www.dal.ca/dept/university_secretariat/academic-integrity/plagiarism-cheating.html)

---

## **Project Structure**

Like any Android project, this application uses the MVC paradigm:

- **Model**: Data for this application is stored in...
- **View**: Each Android activity has an `XML` page associated with it.
- **Controller*: Each Android activity has an `AppCompatActivity` java object associated with it, and employs one or more service.


## API & Server

<Overview of how the server and API work, and how the Endpoints are used>

## Activities

Listed below are the various activities used in this application.

###
Intent params:

This activity makes use of a to keep track of the time passed since the rating began, and the rating information the user has inputted during their session.

### Media Reports
Intent params: 

### Add Media
Intent params: 


---

# Testing

## Click Streams

A list of the click-streams used during the UX development and testing is found in ./UI_Design/UseCase_Clickstreams.txt

## Emulators and devices used

[1] "Java (programming language)", En.wikipedia.org, 2018. [Online]. Available: https://en.wikipedia.org/wiki/Java_(programming_language).